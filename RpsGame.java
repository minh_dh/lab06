import java.util.Random;

public class RpsGame {
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    private Random rand = new Random();

    public String playRound(String playerChoice) {
        String[] options = { "rock", "paper", "scissor" };
        String ComputerChoice = options[rand.nextInt(3)];
        String ComputerState = "lost";
        if (playerChoice.equals("rock")) {
            if (ComputerChoice.equals("paper")) {
                ComputerState = "won";
            } else if (ComputerChoice.equals("rock")) {
                ComputerState = "tied";
            }
        } else if (playerChoice.equals("paper")) {
            if (ComputerChoice.equals("scissor")) {
                ComputerState = "won";
            } else if (ComputerChoice.equals("paper")) {
                ComputerState = "tied";
            }
        } else if (playerChoice.equals("scissor")) {
            if (ComputerChoice.equals("rock")) {
                ComputerState = "won";
            } else if (ComputerChoice.equals("scissor")) {
                ComputerState = "tied";
            }
        } else {
            System.out.println("Invalid player choice.");
        }
        if (ComputerState.equals("lost")) {
            this.wins++;
        }
        if (ComputerState.equals("won")) {
            this.losses++;
        }
        if (ComputerState.equals("tied")) {
            this.ties++;
        }
        return "Computer plays " + ComputerChoice + " and the computer " + ComputerState;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getTies() {
        return ties;
    }

}