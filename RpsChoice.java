import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {

    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String choice;
    private RpsGame game;

    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String choice, RpsGame game) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.choice = choice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent arg0) {
        String messageText = game.playRound(choice);
        this.message.setText(messageText);
        this.wins.setText("wins: " + game.getWins());
        this.losses.setText("losses: " + game.getLosses());
        this.ties.setText("ties: " + game.getTies());
    }

}
