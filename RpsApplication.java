import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
	private RpsGame game = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group();

		// scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);

		// associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);

		VBox overall = new VBox();

		HBox buttons = new HBox();
		Button rockButton = new Button("rock");
		Button paperButton = new Button("paper");
		Button scissorButton = new Button("scissor");

		buttons.getChildren().addAll(rockButton, paperButton, scissorButton);

		HBox textFields = new HBox();
		TextField message = new TextField("Welcome!");
		TextField wins = new TextField("wins: 0");
		TextField losses = new TextField("losses: 0");
		TextField ties = new TextField("ties: 0");
		message.setPrefWidth(300);
		wins.setPrefWidth(100);
		losses.setPrefWidth(100);
		ties.setPrefWidth(100);

		rockButton.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", game));
		paperButton.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", game));
		scissorButton.setOnAction(new RpsChoice(message, wins, losses, ties, "scissor", game));

		textFields.getChildren().addAll(message, wins, losses, ties);

		overall.getChildren().addAll(buttons, textFields);

		root.getChildren().add(overall);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
